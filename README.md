<p align="center">
  <h1 align="center">API REST - Code Challenge</h1>
</p>  


# Sobre

Desafio "Code Challenge" processo seletivo da Digital Republic, foi utilizado as seguintes técnologias!

<ol>
 	<li>Express</li>
	<li>ORM - Sequelize</li>
	<li>Database - Mysql2</li>
 	<li>Validate - Joi</li>
 	<li>Test - Jest</li>
</ol>

### O que deveria ser desenvolvido
Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L

Regras

### Regras de negócio

1 - A parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas pode possuir alturas e larguras diferentes<br>
2 - O total de área das portas e janelas deve ser no máximo 50% da área de parede<br>
3 - A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta<br>
4 - Cada janela possui as medidas: 2,00 x 1,20 mtos<br>
5 - Cada porta possui as medidas: 0,80 x 1,90<br>
6 - Cada litro de tinta é capaz de pintar 5 metros quadrados<br>
7 - Não considero teto nem piso.<br>
8 - As variações de tamanho das latas de tinta são:<br>

<ol>
	<li>0,5 L</li>
 	<li>2,5 L</li>
 	<li>3,6 L</li>
 	<li>18 L</li>
</ol>


# Pré-requisitos:

### Instalando e executando a API localmente:

Você precisará instalar em seu PC: <br>

[Node.js](https://nodejs.org/en/) ✔ <br>
[VSCode](https://code.visualstudio.com/) ✔ <br>
[Insomnia](https://insomnia.rest/download) ✔ <br>

# Passo a Passo
## no terminal digite os comandos:

1 - git clone git@gitlab.com:GabrielModa/code-challenge.git<br>
2 - cd app/backend<br>
3 - npm install: Para instalar as dependencias<br>
4 - npm start: Para iniciar o Projeto<br>


# Rota da API

## Endereço

<b>[GET] </b>https://localhost:3001/sales, irá retornar status code 200, com a informação de quantos baldes de lata serão necessários. <br>

### Schema da requisição 

```javascript
{
"walls":[
   {
     "name": "A",
     "height": 20.21,
     "width": 2,
     "numberOfPorts": 0,
     "numberOfWindows": 0
   },
   {
     "name": "B",  
     "height": 24.21,
     "width": 2,
     "numberOfPorts": 0,
     "numberOfWindows": 0
   },
   {
     "name": "C",
     "height": 24.21,
     "width": 2,
     "numberOfPorts": 0,
     "numberOfWindows": 0
   },
   {
     "name": "D",
     "height": 24.21,
     "width": 2,
     "numberOfPorts": 0,
     "numberOfWindows": 0
   }
]
}
```
<h4>Observações:</h4>
O campo "name", não é obrigatório mas serve para auxiliar na referencia de cada parede, os outros campos são obrigatórios!

### Retorno da requisição 

```javascript
{
  "bucket 18L": 2,
  "bucket 3.6L": 0,
  "bucket 2.5L": 0,
  "bucket 0.5L": 3
}

```

## Endereço

<b>[GET] </b>https://localhost:3001/paints, irá retornar status code 200, todas as variações de tintas cadastradas. <br>
<b>[POST] </b>https://localhost:3001/paints, irá retornar status code 200, todas as variações de tintas cadastradas. <br>

### Retorno da requisição 

```javascript
[
  {
    "id": 1,
    "liter": 0.5
  },
  {
    "id": 2,
    "liter": 2.5
  },
  {
    "id": 3,
    "liter": 3.6
  },
  {
    "id": 4,
    "liter": 18
  }
]
```

## Endereço

<b>[POST] </b>https://localhost:3001/paints, irá retornar status code 200, todas as variações de tintas cadastradas. <br>

### Schema da requisição 

```javascript
  {
    "liter": 0.1
  }
```

### Retorno da requisição 

```javascript
 'Registered'
```

# Rodando teste

Para rodar os teste, basta rodar o comando abaixo:

```javascript
npm test
```

