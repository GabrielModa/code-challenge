const { paintPail } = require('../database/models');

const getAll = async () => paintPail.findAll();
const newBucket = async (liter) => paintPail.create({ liter });

module.exports = { getAll, newBucket };
