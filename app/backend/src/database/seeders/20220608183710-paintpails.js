'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.bulkInsert('paintPails',
    [
      {
        liter: 0.5,     
      },
      {
        liter: 2.5,          
      },
      {
        liter: 3.6,          
      },
      {
        liter: 18,          
      },    
    ], {}),

  down: async (queryInterface) => queryInterface.bulkDelete('paintPails', null, {}),
};