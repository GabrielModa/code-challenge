const paintPail = (sequelize, DataTypes) => {
  const paintPail = sequelize.define('paintPail', {
    liter: DataTypes.FLOAT,
  },
  {
    timestamps: false,
    tableName: 'paintPails',
    underscored: true,
  }
  );

  return paintPail;
};

module.exports = paintPail;
