module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('paintPails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      liter: {
        type: Sequelize.FLOAT,
        allowNull: false,
        unique: true,
      },
    });
  },
  async down(queryInterface, _Sequelize) {
    await queryInterface.dropTable('paintPails');
  },
};
