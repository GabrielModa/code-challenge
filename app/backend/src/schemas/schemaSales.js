const Joi = require('joi');

const salesSchema = Joi.array().items(Joi.object({
  name: Joi.string().messages({
    'string.base': '400|"name" must be a string',
    'string.empty': '400|"name" is not allowed to be empty',

  }),
  height: Joi.number().positive().required().messages({
    'number.base': '400|"height" must be a positive number',
    'number.positive': '400|"height" must be a positive number',
    'any.required': '400|"height" is required',

  }),
  width: Joi.number().required().messages({
    'number.positive': '400|"width" must be a positive number',
    'any.required': '400|"width" is required',
  }),
  numberOfPorts: Joi.number().integer().min(0).required()
    .messages({
      'number.min': '400|"numberOfPorts" must be greater than or equal to 0',
      'number.integer': '400|"numberOfPorts" must be an integer',
      'any.required': '400|"numberOfPorts" is required',
    }),
  numberOfWindows: Joi.number().integer().min(0).required()
    .messages({
      'number.min': '400|"numberOfPorts" must be greater than or equal to 0',
      'number.integer': '400|"numberOfWindows" must be an integer',
      'any.required': '400|"numberOfWindows" is required',
    }),
}));

module.exports = salesSchema;
