const paints = require('./paints');

const doorArea = (0.80 * 1.90);
const windowArea = (2 * 1.20);

const verifyWallArea = (walls) => {
  const alloewdWallArea = walls.find((wall) => {
    const maxArea = (wall.height * wall.width > 50);
    const minArea = (wall.height * wall.width < 1);

    const allowedArea = maxArea || minArea;
    return allowedArea;
  });
  return alloewdWallArea;
};

const verifyMinHeight = (walls) => walls.find((wall) => wall.numberOfPorts && (wall.height < 2.20));

const verifyAllow = (walls) => {
  const allowedArea = walls.find((wall) => {
    const halfArea = (wall.height * wall.width) / 2;
    const amountDoorArea = (wall.numberOfPorts * doorArea);
    const amountWindowArea = (wall.numberOfWindows * windowArea);

    return halfArea < (amountDoorArea + amountWindowArea);
  });
  return allowedArea;
};

const verifyTotalArea = (walls) => {
  const totalArea = walls.map((wall) => {
    const wallArea = (wall.height * wall.width);
    const amountDoorArea = (wall.numberOfPorts * doorArea);
    const amountWindowArea = (wall.numberOfWindows * windowArea);

    return wallArea - (amountDoorArea + amountWindowArea);
  }).reduce((wallAtt, wallCurr) => wallAtt + wallCurr);

  return totalArea;
};

const numberPaintBuckets = async (totalArea) => {
  const paintBuckets = await paints.getAll();

  const liters = paintBuckets.map((bucket) => bucket.liter);
  const orderBuckets = liters.sort((a, b) => b - a);

  const area = totalArea;

  let buckets = {};

  const restBuckets = orderBuckets.reduce((rest, current) => {
    const paintableArea = current * 5;
    const amountBucket = Math.floor(rest / paintableArea);

    const nameBucket = `bucket ${current}L`;
    buckets[nameBucket] = amountBucket;

    const restPaintableArea = rest % paintableArea;
    return restPaintableArea;
  }, area);

  if (restBuckets) {
    const bucket = `bucket ${orderBuckets[orderBuckets.length - 1]}L`;

    buckets = { ...buckets, [bucket]: buckets[bucket] + 1 };
  }

  return buckets;
};

module.exports = {
  verifyMinHeight, verifyWallArea, verifyAllow, verifyTotalArea, numberPaintBuckets,
};
