const paints = require('./paints');
const sales = require('./sales');

module.exports = { paints, sales };
