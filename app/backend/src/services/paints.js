const paints = require('../models/paints');

const getAll = async () => paints.getAll();
const newBucket = async (liter) => paints.newBucket(liter);

module.exports = { getAll, newBucket };
