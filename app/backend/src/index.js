require('dotenv').config();
require('express-async-errors');

const express = require('express');

const app = express();
app.use(express.json());

const errorHandler = require('./middlewares/errorHandler');
const { paintsRouter, salesRouter } = require('./routes/index.routes');

app.use('/paints', paintsRouter);
app.use('/sales', salesRouter);

app.use(errorHandler);

const { PORT } = process.env;
app.listen(PORT || 3001, () => {
  console.log(`Application is running ${PORT}`);
});
