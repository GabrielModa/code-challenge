const service = require('../services/sales');

const getSales = async (req, res) => {
  const { walls } = req.body;

  const verifyWallArea = service.verifyWallArea(walls);

  if (verifyWallArea) {
    return res.status(400).json(`Ops, the wall "${verifyWallArea.name}" area is: ${verifyWallArea.height * verifyWallArea.width}m²... The allowed area is between: 1m² to 50m²!`);
  }

  const verifyMinHeight = service.verifyMinHeight(walls);

  if (verifyMinHeight) return res.status(400).json('Minimum height allowed when there is a door is 2.20! (Very low)');

  const verifyAllow = service.verifyAllow(walls);

  if (verifyAllow) return res.status(400).json('The sum of the doors and windows areas, greater than 50%');

  const verifyTotalArea = service.verifyTotalArea(walls);
  const numberPaintBuckets = await service.numberPaintBuckets(verifyTotalArea);

  return res.status(200).json(numberPaintBuckets);
};

module.exports = { getSales };
