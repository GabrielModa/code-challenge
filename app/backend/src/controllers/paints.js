const paints = require('../services/paints');

const getAll = async (_req, res) => {
  const paintBuckets = await paints.getAll();

  return res.status(200).json(paintBuckets);
};

const newBucket = async (req, res) => {
  const { liter } = req.body;

  await paints.newBucket(liter);

  return res.status(201).send('Registered');
};

module.exports = { getAll, newBucket };
