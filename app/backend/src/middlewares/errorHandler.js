/* eslint-disable no-unused-vars */
const errorHandler = (err, _req, res, _next) => {
  res.status(500).json({ message: 'Ops, error...' });
};

module.exports = errorHandler;
