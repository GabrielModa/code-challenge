const schema = require('../schemas/schemaSales');

const validateSchemaMiddlewares = (req, res, next) => {
  const { walls } = req.body;
  const { error } = schema.validate(walls);

  if (error) {
    const [code, message] = error.message.split('|');
    return res.status(code).json({ message });
  }
  return next();
};

module.exports = validateSchemaMiddlewares;
