const service = require('../services/sales');

describe('Test can bucket quantity function', () => {
  it('returns right value with 150m²', async () => {
    const numberPaintBuckets = await service.numberPaintBuckets(150);

    expect(numberPaintBuckets).toStrictEqual({
      'bucket 18L': 1, 'bucket 3.6L': 3, 'bucket 2.5L': 0, 'bucket 0.5L': 3,
    });
  });
  it('returns right value with 2.6m²', async () => {
    const numberPaintBuckets = await service.numberPaintBuckets(2.6);

    expect(numberPaintBuckets).toStrictEqual({
      'bucket 18L': 0, 'bucket 3.6L': 0, 'bucket 2.5L': 0, 'bucket 0.5L': 2,
    });
  });
});
