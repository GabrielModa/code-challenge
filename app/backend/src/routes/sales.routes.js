const express = require('express');
const { sales } = require('../controllers');
const validateSchemaMiddlewares = require('../middlewares/validateSales');

const router = express.Router();

router.get('/', validateSchemaMiddlewares, sales.getSales);

module.exports = router;
