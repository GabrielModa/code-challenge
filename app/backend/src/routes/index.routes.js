const paintsRouter = require('./paints.routes');
const salesRouter = require('./sales.routes');

module.exports = { paintsRouter, salesRouter };
