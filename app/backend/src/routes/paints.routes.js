const express = require('express');
const { paints } = require('../controllers');

const router = express.Router();

router.get('/', paints.getAll);
router.post('/', paints.newBucket);

module.exports = router;
